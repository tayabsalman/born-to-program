# Born To Program

Contains all examples and sample programs coded or explained in my videos

## Getting started

To make it easy for you to get reference while you are coding your own programs

## Other Links

Use the built-in continuous integration in GitLab.

- [ ] [Follow me on Insta](https://www.instagram.com/tayab.salman/)
- [ ] [Connect on LinkedIn](https://www.linkedin.com/in/tayabsalman/)
***
